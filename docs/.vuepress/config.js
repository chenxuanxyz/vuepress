module.exports = {
    title: 'YUI',
    description: 'Just playing around',
    port:8081,
    head: [
        ['link', { rel: 'icon', href: '/favicon.ico' }], // 增加一个自定义的 favicon(网页标签的图标)
      ],
    markdown: {
        lineNumbers: true // 代码块显示行号
    },
    themeConfig: {
        sidebarDepth: 2,
        nav: [
            {
              text: 'Languages',
              items: [
                { text: 'Chinese', link: '/language/chinese/' },
                { text: 'Japanese', link: '/language/japanese/' }
              ]
            },{
                text: '关于我们',link:'/components/1'
            }
        ],
        sidebar: [
            {
                title: '应用组件',
                collapsable: true, //是否展开
                children:[
                    './pages/yui/1',
                    './pages/yui/2',
                    './pages/yui/3',
                    './pages/yui/4',
                ]
                
            },
            {
                title: '基础组件',
                collapsable: true, //是否展开
                children:[
                    './pages/yui/1',
                    './pages/yui/2',
                    './pages/yui/3',
                    './pages/yui/4',
                ]
                
            },
            {
                title: '微前端',
                collapsable: true, 
                children:[
                    './pages/micro/qiankun',
                    './pages/micro/mf'
                ]
            },{
                title: '脚手架工具',
                collapsable: true, 
                children:[
                    './pages/cli/1',
                ]
            }
        ]
    }
}